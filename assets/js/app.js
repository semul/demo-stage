function get_properties() {
  let base_url = "http://18.141.209.24/api";
  let properties_endpoint = "/properties";

  let callable_url = base_url + properties_endpoint;

  var properties_list = jQuery("#properties");

  var images = [
    "property",
    "house",
    "residence",
    "interior",
    "suites",
    "bedroom",
    "room",
    "kitchen",
    "flat",
    "building",
    "landmark",
  ];

  jQuery.getJSON(callable_url, {}, function (res) {
    properties_list.empty();
    properties_list.append('<div class="row" id="theWrap">');
    if (res.data.length >= 1) {
      jQuery.each(res.data, function (i, item) {
        jQuery("#theWrap").append(
          jQuery('<div class="col-md-3">').html(
            '<div class="property"> <div class="card mb-2"> <img class="card-img-top" src="https://source.unsplash.com/640x480/?' +
              images[i] +
              '" alt="Random Photos"/> <div class="card-body"> <h5 class="card-title">' +
              item.title +
              '</h5> <p class="card-text">' +
              item.description +
              '</p> <hr class"hr"><small>' +
              item.currency +
              " " +
              item.price +
              '</small><br><br><a href="#" class="btn btn-sm btn-primary"> View Property </a> </div></div></div></div>'
          )
        );
        //   alert("Trying");
      });
    } else {
      jQuery("#theWrap").append(
        jQuery('<div class="col-md-12">').html(
          '<div class="property"> <div class="card mt-5 mb-2 p-5"><h1 class="text-center">Listing Not Found</h1></div></div>'
        )
      );
    }
    // properties_list.html('<div class="loading">Loading...</div>');
    // alert("Loading");
  });
}

function load_properties() {
  get_properties();
}
